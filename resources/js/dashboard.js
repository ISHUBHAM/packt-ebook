var $previousBook;
var topOffset = 20;

/* ISOTOPE INIT */
$(".grid").isotope({ itemSelector: ".book", layoutMode: "packery" });

/* GENRE FILTERING */
$(".filters li").on("click", function () {
  var newFilter =
    $(this).data("genre") == "*" ? "*" : "." + $(this).data("genre");

  $(".grid").isotope({ filter: newFilter });
  $(".book-details").trigger("click"); // force book details to close
});

/* BOOK DETAILS TOGGLE */
$(".book").on("click", function () {
  var $currentBook = $(this);
  var $currentBookDetails = $($(this).parent().find(".book-details"));
  var $currentGrid = $($(this).parent());

  // Only animate stamping if this is a different book than the last book
  if (!$previousBook || $currentBook.attr("id") != $previousBook.attr("id")) {
    $currentGrid.isotope("unstamp", $currentBookDetails);
    // Adjust layout only if top position of books differs (e.g., different rows)
    if (
      $previousBook &&
      $previousBook.position().top != $currentBook.position().top
    ) {
      $currentGrid.isotope({ transitionDuration: "0s" });
      $currentGrid.isotope("layout");
    }

    setTimeout(function () {
      var positionTop = $currentBook.position().top;
      var padding = 20;
      var $caret = $($currentBookDetails.find(".caret"));
      var $bookDescription = $($currentBookDetails.find(".book-description"));

      $currentBookDetails.css({
        top: positionTop + $currentBook.height() + padding
      });
      $currentBookDetails.addClass("active");
      $currentBookDetails.show();
      $currentBookDetails.find(".title").html($currentBook.attr("id"));
      $bookDescription.html(GetRandomText());
      $currentGrid.isotope("stamp", $currentBookDetails);
      $currentGrid.isotope("layout");
      $caret.css({ left: $currentBook.position().left + 35 });
      // Animate scroll of books to top of page.
      $("html,body").animate(
        { scrollTop: $currentBook.offset().top - topOffset },
        "slow"
      );
    }, 50);
  }

  // Close the book details if the current book clicked on is the same as previous book
  if ($previousBook && $currentBook.attr("id") == $previousBook.attr("id")) {
    $currentBookDetails.trigger("click");
    $currentBook = null;
  }

  // Update the previous book reference to the current book
  $previousBook = $currentBook;
});

/* HIDE BOOK DETAILS */
$(".book-details").on("click", function () {
  var currentGrid = $(this).parent();
  var $currentBookDetails = $(this);
  $(this).fadeOut(200, function () {
    $currentBookDetails.removeClass("active");
  });

  $(currentGrid).isotope("unstamp", $currentBookDetails);
  setTimeout(function () {
    $(currentGrid).isotope("layout");
  }, 250);
});

function GetRandomText() {
  // return a random number of paragraphs from the Ipsum block
  // min. 2 paragraphs
  var ogContent = $("#lorem").html();
  var paragraphCount = Math.floor(Math.random() * 3) + 1;
  var results = "";
  for (var i = 0; i < paragraphCount; i++) {
    var startIndex = Math.floor(Math.random() * $("#lorem p").length);
    var result = $("#lorem p").slice(startIndex, startIndex + 1);
    results += "<p>" + $(result[0]).html() + "</p>";
  }

  $("#lorem").html(ogContent); // set lorem element's html back to the original
  return results;
}
