# Packt-ebook

## Getting started

To make it easy for you to get started with GitLab, here's a list of recommended next steps.

## Add your files
```
git clone <repo> <directory>
cd <directory>
git branch 
git pull origin origin <branch>
```

## setup .env file

- APP_NAME=Packt
APP_ENV=local
APP_KEY=base64:EhFxM74PB69qmeoNvdb0JJSSOjnbCXow01b1/dEcvRk=
APP_DEBUG=true
APP_URL=http://localhost

LOG_CHANNEL=stack
LOG_DEPRECATIONS_CHANNEL=null
LOG_LEVEL=debug

DB_CONNECTION=mysql
DB_HOST=127.0.0.1
DB_PORT=3306
DB_DATABASE=packt_ebook
DB_USERNAME=root
DB_PASSWORD=password

BROADCAST_DRIVER=log
CACHE_DRIVER=file
FILESYSTEM_DRIVER=local
QUEUE_CONNECTION=sync
SESSION_DRIVER=database
SESSION_LIFETIME=120

MEMCACHED_HOST=127.0.0.1

REDIS_HOST=127.0.0.1
REDIS_PASSWORD=null
REDIS_PORT=6379

MAIL_MAILER=smtp
MAIL_HOST=smtp.mailtrap.io
MAIL_PORT=2525
MAIL_USERNAME=0599a2d7302910
MAIL_PASSWORD=488eaaa51f4582
MAIL_ENCRYPTION=tls

AWS_ACCESS_KEY_ID=
AWS_SECRET_ACCESS_KEY=
AWS_DEFAULT_REGION=us-east-1
AWS_BUCKET=
AWS_USE_PATH_STYLE_ENDPOINT=false

PUSHER_APP_ID=
PUSHER_APP_KEY=
PUSHER_APP_SECRET=
PUSHER_APP_CLUSTER=mt1

MIX_PUSHER_APP_KEY="${PUSHER_APP_KEY}"
MIX_PUSHER_APP_CLUSTER="${PUSHER_APP_CLUSTER}"




# composer install/update 

It basically install the project depedency in your system.
Files to upload in storage path - https://drive.google.com/file/d/1cvueglpaHQiNTQeKxhh8Qb_aDHPFOAZQ/view?usp=sharing 

## Technology Used
-Laravel Jetstream
-Livewire
-Php
-Blade template 
-alpine js
-tailwind css

## Brife
Laravel Jetstream is a beautifully designed application starter kit for Laravel and provides the perfect starting point for your next Laravel application. Jetstream provides the implementation for your application's login, registration, email verification, two-factor authentication, session management, API via Laravel Sanctum, and optional team management features.

Jetstream is designed using Tailwind CSS and offers your choice of Livewire or Inertia scaffolding.


## Description
Laravel Livewire is a library that makes it simple to build modern, reactive, dynamic interfaces using Laravel Blade as your templating language. This is a great stack to choose if you want to build an application that is dynamic and reactive but don't feel comfortable jumping into a full JavaScript framework like Vue.js.

When using Livewire, you may pick and choose which portions of your application will be a Livewire component, while the remainder of your application can be rendered as the traditional Blade templates you are used to.


## License
For open source projects, say how it is licensed.

## Project status
Packt ebook-self in which you can get the book details with different kind of book catagory with realtive technolgy stack.
