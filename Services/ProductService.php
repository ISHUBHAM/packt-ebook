<?php

namespace Modules\Organizations\Services;


use Illuminate\Routing\Controller;
use Modules\Organizations\Repositories\v1\OrganizationRepository;
use DateTime;

class OrganizationService extends Controller
{
    
    /**
     * OrganizationRepository instance
     * @var $organizationRepository
     */
    protected $organizationRepository;

    /**
     * OrganizationRepository constructor.
     * @param organizationRepository $organizationRepository
     */
    public function __construct(OrganizationRepository $organizationRepository)
    {
        $this->organizationRepository = $organizationRepository;
    }

    /**
     *
     * @param  $data
     * @return object
     */
    public function getOrganizationList($data){
        try {
            $organizationLists = $this->organizationRepository->getOrganizationList($data);
            return $organizationLists;
        } catch(\Exception $e) {
            dd($e);
            return false;
        }
        
    }

}