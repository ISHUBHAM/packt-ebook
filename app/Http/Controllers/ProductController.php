<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;


class ProductController extends Controller
{

    /**
     * Getting all book listing
     *
     * @param Request $request
     * @return void
     */
    public function dashboard(Request $request)
    {   
            $detail = DB::table('books')->select('title','author','publication_date','categories','product_type','book_length','packt_rank','reviews_count','reviews_rating','product_url')->get();
            $arrData = [];
            foreach($detail as $key => $val)
            {
                $arrData[$key] = $val;
            }
            // return $arrData;
        // $data = array('arrData' => $arrData);
        return view('dashboard')->with(['data'=> $arrData]);
    }

    /**
     * Getting book list by catagory
     *
     * @param Request $request
     * @return void
     */
    public function webdevlopment(Request $request)
    {   
            $detail = DB::table('books')->where('categories','Web Development')->select('title','author','publication_date','categories','product_type','book_length','packt_rank','reviews_count','reviews_rating','product_url')->get();
            $arrData = [];
            foreach($detail as $key => $val)
            {
                $arrData[$key] = $val;
            }
            // return $arrData;
        // $data = array('arrData' => $arrData);
        return view('dashboard')->with(['data'=> $arrData]);
    }


    public function data(Request $request)
    {   
            $detail = DB::table('books')->where('categories','data')->select('title','author','publication_date','categories','product_type','book_length','packt_rank','reviews_count','reviews_rating','product_url')->get();
            $arrData = [];
            foreach($detail as $key => $val)
            {
                $arrData[$key] = $val;
            }
            // return $arrData;
        // $data = array('arrData' => $arrData);
        return view('dashboard')->with(['data'=> $arrData]);
    }


    public function programming(Request $request)
    {   
            $detail = DB::table('books')->where('categories','programming')->select('title','author','publication_date','categories','product_type','book_length','packt_rank','reviews_count','reviews_rating','product_url')->get();
            $arrData = [];
            foreach($detail as $key => $val)
            {
                $arrData[$key] = $val;
            }
            // return $arrData;
        // $data = array('arrData' => $arrData);
        return view('dashboard')->with(['data'=> $arrData]);
    }


    public function mobile(Request $request)
    {   
            $detail = DB::table('books')->where('categories','mobile')->select('title','author','publication_date','categories','product_type','book_length','packt_rank','reviews_count','reviews_rating','product_url')->get();
            $arrData = [];
            foreach($detail as $key => $val)
            {
                $arrData[$key] = $val;
            }
            // return $arrData;
        // $data = array('arrData' => $arrData);
        return view('dashboard')->with(['data'=> $arrData]);
    }


    public function security(Request $request)
    {   
            $detail = DB::table('books')->where('categories','security')->select('title','author','publication_date','categories','product_type','book_length','packt_rank','reviews_count','reviews_rating','product_url')->get();
            $arrData = [];
            foreach($detail as $key => $val)
            {
                $arrData[$key] = $val;
            }
            // return $arrData;
        // $data = array('arrData' => $arrData);
        return view('dashboard')->with(['data'=> $arrData]);
    }


    public function business(Request $request)
    {   
            $detail = DB::table('books')->where('categories','Business & Other')->select('title','author','publication_date','categories','product_type','book_length','packt_rank','reviews_count','reviews_rating','product_url')->get();
            $arrData = [];
            foreach($detail as $key => $val)
            {
                $arrData[$key] = $val;
            }
            // return $arrData;
        // $data = array('arrData' => $arrData);
        return view('dashboard')->with(['data'=> $arrData]);
    }


    public function gamedevelopment(Request $request)
    {   
            $detail = DB::table('books')->where('categories','Game Development')->select('title','author','publication_date','categories','product_type','book_length','packt_rank','reviews_count','reviews_rating','product_url')->get();
            $arrData = [];
            foreach($detail as $key => $val)
            {
                $arrData[$key] = $val;
            }
            // return $arrData;
        // $data = array('arrData' => $arrData);
        return view('dashboard')->with(['data'=> $arrData]);
    }


    public function getTabDetails(Request $request)
    {  
        try
        {
            $detail = DB::table('books')->select('title','author','publication_date','categories','product_type','book_length','packt_rank','reviews_count','reviews_rating','product_url')->get();
            $arrData = [];
            foreach($detail as $key => $val)
            {
                $arrData[$key] = $val;
            }
            return response()->json(['response' => ['code' =>'200', 'message' =>'Book list fetch successfully', 'data' =>$arrData]]);

        }
        catch (\Exception $e) {
            Log::error(
                'Failed to fetch data from db',
                ['message' => $e->getMessage(), 'trace' => $e->getTraceAsString()]
            );
        }

    }

}
