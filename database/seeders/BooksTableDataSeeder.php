<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Http;

class BooksTableDataSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //Pass catagory param
        //Web%20Development
        // Programming
        // Cloud%20&%20Networking
        // Mobile
        // Security
        // Business%20&%20Other
        // Game%20Development
        $maxCount = 20;
        $token = "agCKhwv5C64gYlBccIquQDW0LzCxhDIbGby8QdJl";
        $packtRecord = Http::get('https://www.packtpub.com/api/products/category/Mobile', [
    		'token' => $token
		]);
        // return $packtRecord;
        foreach($packtRecord['data'] as $key =>$pdt)
        {
            // dd($pdt);
            // $pdt['title'],$pdt['authors'][0],$pdt['publication_date'],$pdt['categories'][0],$pdt['language'],$pdt['vendor']
            $insert = DB::table('books')->insert([
                'title'=>$pdt['title'],
                'author'=>$pdt['author'],
                'publication_date'=>$pdt['publication_date'],
                'categories'=>$pdt['category'],
                'product_type'=>$pdt['product_type'],
                'book_length'=>$pdt['book_length'],
                'packt_rank'=>$pdt['packt_rank'],
                'product_url'=>$pdt['product_url'],
                'reviews_count'=>$pdt['reviews_count'],
                'reviews_rating'=>$pdt['reviews_rating'],
            ]);
        }
    }
}
