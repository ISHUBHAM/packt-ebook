<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\ProductController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {return view('welcome');});

Route::middleware(['auth:sanctum',config('jetstream.auth_session'),'verified'])->group(function () 
{
    // Route::get('/dashboard', function () {return view('dashboard');})->name('dashboard');
    Route::get('/dashboard',[ProductController::class, 'dashboard'])->name('dashboard');
    Route::get('/webdevlopment',[ProductController::class, 'webdevlopment'])->name('webdevlopment');
    Route::get('/data',[ProductController::class, 'data'])->name('data');
    Route::get('/programming',[ProductController::class, 'programming'])->name('programming');
    Route::get('/mobile',[ProductController::class, 'mobile'])->name('mobile');
    Route::get('/security',[ProductController::class, 'security'])->name('security');
    Route::get('/business',[ProductController::class, 'business'])->name('business');
    Route::get('/gamedevelopment',[ProductController::class, 'gamedevelopment'])->name('gamedevelopment');


    // Route::post('/getProductData', [ProductController::class, 'getTabDetails'])->name('getTabDetails');
});
